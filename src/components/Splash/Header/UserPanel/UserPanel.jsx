import React from 'react'
import { Switch, Route } from 'react-router-dom'
//import icons
import { FaToggleOff, FaToggleOn } from "react-icons/fa";
import { FaRegPlusSquare } from 'react-icons/fa';


// -- DEFINE COMPONENTS
// wrapper component
class UserPanel extends React.Component {
  constructor(props) {
    super(props);
    this.toggleUserPanel = this.toggleUserPanel.bind(this);
    this.state = {
      isUserPanelToggled: false
    };
  }
  toggleUserPanel() {
    switch (this.state.isUserPanelToggled) {
      case false:
        this.setState({ isUserPanelToggled: true })
        break;
      case true:
        this.setState({ isUserPanelToggled: false })
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <>
        <Button isUserPanelToggled={this.state.isUserPanelToggled} toggleUserPanel={this.toggleUserPanel} />   {/*if false*/}
        <Panel isUserPanelToggled={this.state.isUserPanelToggled} toggleUserPanel={this.toggleUserPanel} />    {/*if true*/}
      </>
    )
  }
}
// toggle component
function Button(props) {
  if (props.isUserPanelToggled == false) {
    return (<i onClick={props.toggleUserPanel}><FaRegPlusSquare /></i>);
  } else {
    return null;
  }
}
//  userpanel component
function Panel(props) {
  if (props.isUserPanelToggled == true) {
    return (
      <>
        <i onClick={props.toggleUserPanel}><FaRegPlusSquare /></i>
        <img src="https://scontent.fsof6-1.fna.fbcdn.net/v/t1.0-1/c108.110.432.432/s24x24/14671179_1581492595209470_6096429876448043120_n.jpg?_nc_cat=109&_nc_ht=scontent.fsof6-1.fna&oh=20085d687be4b9ebc04abd540503de53&oe=5C45107A" alt=""/>
      </>
    )
  } else {
    return null;
  }
}


export default UserPanel;