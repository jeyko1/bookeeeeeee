import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import { connect } from 'react-redux'
// import components
import Logo from './Logo.jsx';
import UserPanel from './UserPanel/UserPanel.jsx';
// import redux action
import { setUser } from './../../../actions/userActions';
// import icons
import { IoIosHeart } from 'react-icons/io';
import { IoMdGitCompare } from 'react-icons/io';
import { FaRegPlusSquare } from 'react-icons/fa';
// --
class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {

        return (
            <>
                <Link exac to='./'><Logo /></Link>
                <Link to='./favorites'><IoIosHeart /></Link>
                <Link to='./createlisting'><FaRegPlusSquare /></Link>
                <UserPanelWrapper loggedIn={this.props.user.loggedIn} />
            </>
        )
    }
}

function UserPanelWrapper(props) {
    if (props.loggedIn == true) {
        return (<UserPanel />
        );
    } else if (props.loggedIn == false) {
        return (<Link to='/register'><IoMdGitCompare /></Link>)
    }
}



const mapStateToProps = state => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Component);