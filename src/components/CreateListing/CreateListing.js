import React from 'react'
import { Switch, Route } from 'react-router-dom'
import PlacesAutocomplete from 'react-places-autocomplete';
import {
    geocodeByAddress,
    geocodeByPlaceId,
    getLatLng,
} from 'react-places-autocomplete';

class CreateListing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            home_types: ['house', 'flat', 'condo', 'studio'],
            selected_home_type: 'house',
            beds_count: [1,2,3,4,5,6],
            selected_beds_count: '1',
            people_count: [1,2,3,4,5,6],
            selected_people_count: '1',
            amenities: ['amenity1', 'amenity2', 'amenity3', 'amenity4' ]
        };
    }
    handleChange = address => {
        this.setState({ address });
    };

    handleSelect = address => {
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => console.log('Success', latLng))
            .catch(error => console.error('Error', error));
    };

    handleProceed = () => {

    }
    render() {
        return (
            <>
                <PlacesAutocomplete
                    value={this.state.address}
                    onChange={this.handleChange}
                    onSelect={this.handleSelect}
                >
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <div>
                            <input
                                {...getInputProps({
                                    placeholder: 'Where to?',
                                    className: 'location-search-input',
                                })}
                            />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                        ? 'suggestion-item--active'
                                        : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div
                                            {...getSuggestionItemProps(suggestion, {
                                                className,
                                                style,
                                            })}
                                        >
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    )}
                </PlacesAutocomplete>
                What kind of housing are you offering?
                <select  value={this.state.selected_home_type} onChange={e=> this.setState({selected_home_type: e.target.value})}>
                    {this.state.home_types.map((item) => {
                        return(
                            <option value={item}>{item}</option>
                            )
                        })}
                </select>
                How much beds are available
                <select value={this.state.selected_beds_count} onChange={e=> this.setState({selected_beds_count: e.target.value})}>
                    {this.state.beds_count.map((item) => {
                        return(
                            <option value={item}>{item}</option>                            
                        )
                    })}
                </select>
                How much people can you accept?
                <select value={this.state.selected_people_count} onChange={e=> this.setState({selected_people_count: e.target.value})}>
                    {this.state.people_count.map((item) => {
                        return(
                            <option value={item}>{item}</option>                            
                        )
                    })}
                </select>
                How much people can you accept?
                <select value={this.state.selected_people_count} onChange={e=> this.setState({selected_people_count: e.target.value})}>
                    {this.state.people_count.map((item) => {
                        return(
                            <option value={item}>{item}</option>                            
                        )
                    })}
                </select>
                Choose amenities: 
                <>
                {this.state.amenities.map((item, index, array) => {
                        return(
                            <i onClick={e=> 
                                this.setState({
                                    amenities: this.state.amenities.filter(item => item !== e.target.value)})
                            }>{item}</i>                    
                        )
                    })}
                    <button onClick={this.setState({amenities})}>Proceed</button>
                    <button onClick={this.handleProceed}>Proceed</button>
                </>
            </>
        )
    }
}

export default CreateListing;