import React, { useState } from 'react'
import { Switch, Route } from 'react-router-dom'
//define component
function Component() {
  //define states
  const [FirstName, setFirstName] = useState('')
  const [LastName, setLastName] = useState('')
  const [UserName, setUserName] = useState('')
  const [EMail, setEMail] = useState('')
  const [Password, setPassword] = useState('')
  const [Password2, setPassword2] = useState('')
  const [Phone, setPhone] = useState('')
  const [Description, setDescription] = useState('')

  const [Status, setStatus] = useState('')

  //define functions
  const _submitLogin = () => {
    fetch('http://159.89.26.144/api/v1/rest-auth/registration/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
      },
      body: {
        "username": FirstName,
        "email": EMail,
        "password1": Password,
        "password2": Password2
      }
    })
      .then((response) => response.json())
      .then((response) => { console.dir(response) })
      .catch(err => {setStatus('Server Error')})
  }
  return (
    <div>
      <form>
        <input type="text" name={FirstName} placeholder='First Name' onChange={e => setFirstName(e.target.value)} />
        <input type="text" name={LastName} placeholder='Last Name' onChange={e => setLastName(e.target.value)} />
        <input type="text" name={UserName} placeholder='Username' onChange={e => setUserName(e.target.value)} />
        <input type="text" name={EMail} placeholder='E-Mail' onChange={e => setEMail(e.target.value)} />
        <input type="password" name={Password} placeholder='Password' onChange={e => setPassword(e.target.value)} />
        <input type="password" name={Password2} placeholder='Repeat password' onChange={e => setPassword2(e.target.value)} />
        <input type="text" name={Phone} placeholder='Phone number' onChange={e => setPhone(e.target.value)} />
        <input type="text" name={Description} placeholder='Short description' onChange={e => setDescription(e.target.value)} />
        <button type='button' onClick={_submitLogin}>Next</button>
      </form>
      {Status}
    </div>
  )

}
export default Component;