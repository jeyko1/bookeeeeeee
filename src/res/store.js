import { createStore, applyMiddleware, compose } from "redux";
import reducer from "../reducers/_index";



// const logger = store => next => action => {
//   console.log(action);
//   next(action);
// };

// const middleware = applyMiddleware(logger);
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const store = createStore(reducer, composeEnhancers(middleware));
const store = createStore(
  reducer, /* preloadedState, */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
// ----------- debug

export default store;
