const initialUserState = {
  loggedIn: false,
  username: 'blank',
  name: '',
  profile_pic: '',
}


const userRed = (state = initialUserState, action) => {
  if (action.type == 'SET_USER'){
    return(
      {
        loggedIn: true,
        username: action.payload.username,
        firstname: action.payload.firstname,
        lastname: action.payload.lastname,
        avatar: action.payload.avatar
      }
    );
  } else {
    return state;
  }
}

export default userRed;