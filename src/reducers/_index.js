import userRed from './userRed';
import { combineReducers } from 'redux';

export default combineReducers({
  user: userRed
})
