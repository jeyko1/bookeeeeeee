import React, { Component, Provider } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route } from 'react-router-dom'
// import routes
import Header from './components/Splash/Header/Header';
import Register from './components/Register/Register';
import CreateListing from './components/CreateListing/CreateListing';
import Favorites from './components/Favorites/Favorites';
import Listing from './components/Listing/Listing';
import Main from './components/Splash/Main/Main';
import Search from './components/Search/Search';
import Footer from './components/Splash/Footer/Footer';
// import context
import { AppCtx } from './AppCtx';

// define app
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      
    }
  }

  render() {
    return (
      <>
        <AppCtx.Provider value={this.state}>
          <Header />
          <Switch>
            <Route exact path='/' component={Main} />
            <Route path='/createlisting' component={CreateListing} />
            <Route path='/favorites' component={Favorites} />
            <Route path='/register' component={Register} />
            <Route path='/search/:query' component={Search} />
            <Route path='/listing/:id' component={Listing} />
          </Switch>
          <Footer />
        </ AppCtx.Provider>
      </>
    );
  }
  //define inital context values
}

export default App;
